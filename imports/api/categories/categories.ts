import { PublicSettings } from '/imports/utils/PublicSettings';

const Categories = { ...PublicSettings.categories };

export default Categories;
